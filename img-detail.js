{
  const {
    html,
  } = Polymer;
  class ImgDetail extends Polymer.Element {

    static get is() {
      return 'img-detail';
    }

    static get properties() {
      return {
        title: {
          type: String,
          value: () => '',
          observer: '_checkTitle'
        },
        text: {
          type: String,
          value: () => '',
          observer: '_checkText'
        }
      };
    }
    static get observers() {
      return [];
    }

    // Verifica si el largo de un titulo es demasiado largo o no existe
    _checkTitle() {
      if (! this.title) {
        this.set('title', '');
      }
      if (this.title.length === 0) {
        this.set('title', 'Titulo no disponible');
      }
      if (this.title.length >= 51) {
        this.set('title', this.title.substring(0, 50).concat('...'));
      }
    }

    // Verifica la existencia de la descripcion
    _checkText() {
      if (! this.text) {
        this.set('text', '');
      }
      if (this.text.length === 0) {
        this.set('text', 'Descripción no disponible');
      }
    }
  }

  customElements.define(ImgDetail.is, ImgDetail);
}